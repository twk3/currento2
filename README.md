# Current O2 DataField

O2 DataField for Garmin ConnectIQ devices

## Description

Current O2 is a Garmin Connect IQ DataFiled that displays the current Oxygen saturation percentage, as available from the Activity.Info API. The API provides the most current data it has available, but note this is almost never actually current. It will show the last calculated reading. How often readings are taken are determined by the pulseOx settings you have specified on your devices, in combination with your stillness. (The pulseOz sensor is only able to capture valid data while you are still).

## Min O2 Alert

This DataField comes with an optional alert that vibrates when the oxygen saturation reading dips below a minimum. This might be useful for you if you want your Garmin Device to wake you up during the night in order to reposition. This might improve your oxygen and sleep quality if you find you have extended periods of low oxygen during the night. 

**NOTE** That both this DataField and your Garmin Device's pulseOx sensors are not intended for medical use. If you think you might be suffering from sleep apnea, you should consult your doctor.

**NOTE** Running an Activity during the night might impact the native Garmin Sleep tracking. See the [limitations section below](#limitations). 

## Settings

The following settings are available:

| Setting | Default Value | Description |
| ------- | ------------- | ----------- |
| minO2   | 88            | The mininum oxygen saturation reading before the alert is fired. |
| vibrate | 50            | The vibration strength percentage of the alert. |

## Limitations

1. Venu SQ
   - The number picker in settings crashes when used.
2. Venu 2 and 2s
   - Running an Activity at night results in no other Native Sleep data being recorded.
3. D2 Air
   - Alerts will vibrate, but show no message.

## License

This code is MIT licensed. See the [LICENSE](LICENSE) file for the full text.

The NumberFactory.mc file is provided under the Connect IQ SDK license agreement. A copy of the License can be found in [source/factories/CIQ-LICENSE-AGREEMENT.html](source/factories/CIQ-LICENSE-AGREEMENT.html)
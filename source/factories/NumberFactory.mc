// This class is a copy of the sample provided by Garmin in their SDK.
// Use of this class is subjec to the Garmin CONNECT IQ SDK License Agreement.
// A copy of the agreement is provided alongside this file.

using Toybox.Graphics;
using Toybox.WatchUi;
import Toybox.Lang;

class NumberFactory extends WatchUi.PickerFactory {
    hidden var mStart as Numeric;
    hidden var mStop as Numeric;
    hidden var mIncrement as Numeric;
    hidden var mFormatString;
    hidden var mFont;

    function getIndex(value) as Numeric {
        var index = (value / mIncrement) - mStart;
        return index;
    }

    function initialize(start, stop, increment, options) {
        PickerFactory.initialize();

        mStart = start;
        mStop = stop;
        mIncrement = increment;

        if(options != null) {
            mFormatString = options.get(:format);
            mFont = options.get(:font);
        }

        if(mFont == null) {
            mFont = Graphics.FONT_NUMBER_HOT;
        }

        if(mFormatString == null) {
            mFormatString = "%d";
        }
    }

    function getDrawable(index, selected) {
        var curValue = getValue(index) as Numeric;
        return new WatchUi.Text( { :text=>curValue.format("%d"), :color=>Graphics.COLOR_WHITE, :font=> mFont, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER } );
    }

    function getValue(index) {
        return mStart + (index * mIncrement) as Numeric;
    }

    function getSize() {
        return ( mStop - mStart ) / mIncrement + 1;
    }

}

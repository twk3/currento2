import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;

class CurrentO2App extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new CurrentO2View() ] as Array<Views or InputDelegates>;
    }

    (:appsettings)
    function getSettingsView() {
        return [new CurrentO2SettingsMenu(), new CurrentO2SettingsMenuDelegate()];
    }

}

function getApp() as CurrentO2App {
    return Application.getApp() as CurrentO2App;
}